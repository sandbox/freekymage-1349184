<?php

/**
 * @file
 *   Implementation of table optimization.
 */

/**
 * Implements hook_drush_command().
 *
 * Adds a new drush command "dbclean-clean" which optimizes database tables.
 */
function dbclean_drush_command() {
  $items = array();
  $items['dbclean-clean'] = array(
    'description' => dt('Optimize drupal tables.'),
    'arguments' => array(
      'minimum' => dt('The minimum amount in kilobytes of unoptimized space that the database needs to have. Default is 0.'),
    ),
    'examples' => array(
      'drush dbcc 5' => dt('Optimize tables if the combined amount of unoptimized space is over 5KB.'),
      'drush dbcc -v' => dt('Show which tables get optimized.'),
    ),
    'aliases' => array('dbcc'),
  );
  return $items;
}

/**
 * Callback function for dbclean-clean drush command.
 *
 * Fetches the current DB schema and
 *
 * @param $minimum int
 *   Minimum amount to optimize in KB.
 */
function drush_dbclean_clean($minimum = 0) {
  // Check database schema.
  $schema = _drush_sql_get_scheme();
  switch ($schema) {
    // Only mysql is supported for now.
    case 'mysql':
    case 'mysqli':
      _dbclean_mysql_clean($minimum);
      break;
    default:
      drush_print(dt('Database schema !value is not supported.', array('!value' => $schema)));
  }
}

/**
 * Helper function for dbclean-clean.
 *
 * @param $minimum int
 *   Minimum amount to optimize in KB.
 */
function _dbclean_mysql_clean($minimum) {
  // Get the tables that can use optimization.
  $table_list = db_query("SHOW TABLE STATUS WHERE Engine = 'MyISAM' AND Data_free > 0");
  // Count the amount of bytes we can optimize.
  $total_bytes = 0;
  // We only need the names of the tables.
  $tables_to_optimize = array();

  while ($result = drush_db_fetch_object($table_list)) {
    $total_bytes += $result->Data_free;
    $tables_to_optimize[] = $result->Name;
  }

  // Don't optimize if the user has giving a minimum.
  if ($total_bytes > $minimum * 1024) {
    // Ask the user if he is sure to continue.
    $confirm = drush_confirm(dt('Do you want to optimize !total_bytes bytes in !number_of_tables table(s)?',
            array('!total_bytes' => $total_bytes, '!number_of_tables' => count($tables_to_optimize))));
    if ($confirm) {
      drush_log(dt('Optimizing tables...'));

      // Do the actual optimzing.
      foreach ($tables_to_optimize as $table) {
        db_query('OPTIMIZE TABLE ' . $table);
      }

      // Extra info when users use verbose mode.
      drush_log(dt('Optimized tables: ' . implode(', ', $tables_to_optimize) . '.'));
      drush_log(dt('Optimizing done.'), 'success');
    }
    else {
      drush_log(dt('Optimizing aborted.'), 'failed');
    }
  }
  else {
    drush_log(dt('Nothing to optimize.'), 'ok');
  }
}
